# How to get images from Bing Bird's Eye

## Basics

**Bird's Eye** is a feature of the web mapping service [Bing Maps](http://virtualearth.net) from Microsoft.
It consists of oblique images that are captured by a low-flying aircraft at an angle of 45 degrees.
Images are captured in four directions: North, East, South and West. 
There is no continuous converage of Bird's Eye imagery, only larger cities are mapped.

## The TileUrl

https://t.ssl.ak.tiles.virtualearth.net/tiles/be{base4sceneId}01{QuadKey}?g=11124

Parameters:

- base4sceneId: ad SceneId
  
- QuadKey: ad QuadKey
  
- g: I don't know what this parameter does bit it doesn't seem to have an effect.

## Metadata Url
Metadata for Scenes is collected using this url:

https://t.ssl.ak.tiles.virtualearth.net/tiles/cmd/BirdsEyeSceneMetaData?north={north}&south={south}&east={east}&west={west}&count=100&dir=

Parameters:

- count: number of values that are returned.

- north, south, east, west: These Parameters specify an area, which is searched for scenes. 

- The Response is in JSON.

Example of https://t.ssl.ak.tiles.virtualearth.net/tiles/cmd/BirdsEyeSceneMetaData?north=52.61961911008604&south=52.41961911008604&east=13.50468722473804&west=13.304687224738041&count=100&dir=&ver=0
```json
[
    {"elapsed":0.0063541},
    {"id":65668873,"la":52.520213863220029,"lo":13.404728884838812,"al":53.158738136291504,"rec":"76401-50465","cd":"\/Date(1597215600000)\/","fra":50465,"cam":2,"camlla":[52.519816348273174,13.370269994541111,2245.9225],"lrgb":5,"size":[14144,10560],"osize":[14144,10560],"gtp":[0.00204378076661893,-0.021033128150540804,0.9997766903065809,-0.70331491405099733,-0.71074994153470084,-0.013514891115266568,-0.71087548461992722,0.70313023553863918,0.01624552977780527],"ptg":[0,0,0,0,0,0,0,0,0],"ullla":[52.527557400028925,13.415231246135715,53.649356842041016],"urlla":[52.513138544955744,13.415800495372345,53.5634765625],"lllla":[52.525274725162475,13.393763730569431,51.276687622070312],"lrlla":[52.514884971335164,13.394120067277765,54.145431518554688],"look":88.910653564782763,"clook":"E","fl":32813.829787234041,"ecc":[1153498591.1739502,704280145.52498555,197748.42457662235],"aoi":"76401","blurred":0,"ver":1,"pids":[236],"em":"s","dis":0},
    {.....},
    {.....}
]
```

The Response is a list of all Scenes as a JSON list. The first Element is always the "elapsed" value.

Paramerters:

- id: SceneId used by the TileUrl. **Id needs the be converted to base4**
 
- la, lo: Latutude, Longitude in EPSG:4326
  
- cd: Recording Date in Unix Time
  
- lrgb, size: Used to calculate image size

- look: direction in which the scene was recorded (degrees)

## The QuadKey
The QuadKey is specifying the position in the Scene. Often not all keys are used.

List of all QuadKeys in order

|     |     |     |     |     |     |     |     |
|-----|-----|-----|-----|-----|-----|-----|-----|
| 000 | 001 | 010 | 011 | 100 | 101 | 110 | 111 |
| 002 | 003 | 012 | 013 | 102 | 103 | 112 | 113 |
| 020 | 021 | 030 | 031 | 120 | 121 | 130 | 131 |
| 022 | 023 | 032 | 033 | 122 | 123 | 132 | 133 |
| 200 | 201 | 210 | 211 | 300 | 301 | 310 | 311 |
| 202 | 203 | 212 | 213 | 302 | 303 | 312 | 313 |
| 220 | 221 | 230 | 231 | 320 | 321 | 330 | 331 |
| 222 | 223 | 232 | 233 | 322 | 323 | 332 | 333 |

## How to get scraping

1. First of all you need to get Metadata.

    * Used the Metadata Url to get metadata. At a minimum you only need the "id", "lrgb" and "size" values, but you can include the other values as well if you need them.
      
    * A too large count parameter or a too large area do not work. For me splitting in 200 parts per latitude/longitude line worked with a count of 100.
  
2. Calculate actual scene size

    * Take the size values: e.g.: [14144, 10560]
  
    * Devide both values by 2^(lrgb-3)
  
    * In this case the correct image size is [3536, 2640]

3. Download tiles

    * A normal tile has a size of 512x512 pixels, a tile that does not exist has a resolution of 256x256 pixels.
  
    * Take the id value from Metadata and convert it to base 4, and add leading zeros until you get 15 digits: 65668873 => 003322200130021
  
    * Insert the values in the Tile Url: https://t.ssl.ak.tiles.virtualearth.net/tiles/be00332220013002101000?g=11124
          000 <img src="https://t.ssl.ak.tiles.virtualearth.net/tiles/be00332220013002101000?g=11124" width=200 higth=200 /> 001 <img src="https://t.ssl.ak.tiles.virtualearth.net/tiles/be00332220013002101001?g=11124" width=200 higth=200 /> ...

4. Combine the Tiles

   * You can use FFmpeg, ImageMagicks montage or any other programm to combine them. (I used Magick)

